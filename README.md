# README #

This is the README file for the Russian Dvorak keyboard layout.

### This repository contains the source for the Russian Dvorak keyboard layout, authored in the [Microsoft Keyboard Layout Creator](https://msdn.microsoft.com/en-us/goglobal/bb964665.aspx) ###

* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Install [Microsoft Keyboard Layout Creator](https://msdn.microsoft.com/en-us/goglobal/bb964665.aspx)
* Clone the repo and open the .klc file in MSKLC
* Check in the source file only.  Executables created by MSKLC can go to the Downloads page.

### Contribution guidelines ###

* Submit pull requests and I will merge your changes into master after I test them.
